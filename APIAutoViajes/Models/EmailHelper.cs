﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Data;

namespace APIAutoViajes.Models
{
    public class EmailHelper
    {
        public static List<EmailAddress> g_list_send =
            new List<EmailAddress> {
                new EmailAddress("lmar11xd@gmail.com")
            };

        public static void SendEmailNuevaAV(int codi_auvi)
        {
            SendGridClient SendGrid_Client = null;

            DataTable l_dt_emailFormat = null, l_dt_email_info = null;

            string l_SendGridKey,
                l_htmlContent,
                l_from,
                l_subject,
                l_formatId,
                l_template,
                l_scheme,
                l_style,
                l_head,
                l_foot;

            try
            {
                l_formatId = Dao.GetParameterByKey("FORMATO_EMAIL_NUEVA_AV");
                l_dt_emailFormat = Dao.GetEmailFormatById(Convert.ToDecimal(l_formatId));

                l_SendGridKey = System.Configuration.ConfigurationManager.AppSettings.Get("SendGrid_ApiKey");

                l_foot = l_dt_emailFormat.Rows[0]["Foot"].ToString();
                l_dt_email_info = Dao.GetEmailInformation();

                l_foot = l_foot.Replace("{sider_web_url}", l_dt_email_info.Rows[0]["SiderperuWebUrl"].ToString())
                    .Replace("{sider_web_text}", l_dt_email_info.Rows[0]["SiderperuWebTexto"].ToString())
                    .Replace("{gerdau_web_url}", l_dt_email_info.Rows[0]["GerdarWebUrl"].ToString())
                    .Replace("{gerdau_web_text}", l_dt_email_info.Rows[0]["GerdarWebTexto"].ToString())
                    .Replace("{app_name}", l_dt_email_info.Rows[0]["VisitaAppName"].ToString())
                    .Replace("{sider_short_name}", l_dt_email_info.Rows[0]["SiderperuShortName"].ToString())
                    .Replace("{visita_phone}", l_dt_email_info.Rows[0]["VisitaPhone"].ToString())
                    .Replace("{visita_anexo}", l_dt_email_info.Rows[0]["VisitaAnexo"].ToString())
                    .Replace("{visita_rpc}", l_dt_email_info.Rows[0]["VisitaRpc"].ToString())
                    .Replace("{label_1}", l_dt_email_info.Rows[0]["VisitaLabel1"].ToString());

                SendGrid_Client = new SendGridClient(l_SendGridKey);

                var l_dt_av = Dao.GetAutViajeEmail(codi_auvi);

                foreach (DataRow row in l_dt_av.Rows)
                {
                    string nro_auvi = row["NRO_AUTORIZACION"].ToString();
                    string nombres = row["NOMBRES"].ToString();
                    string destino = row["DESTINO"].ToString();
                    string f_ini = (Convert.IsDBNull(row["FECHA_INICIO"]) ? default(DateTime) : Convert.ToDateTime(row["FECHA_INICIO"])).ToString("dd/MM/yyyy");
                    string f_fin = (Convert.IsDBNull(row["FECHA_FIN"]) ? default(DateTime) : Convert.ToDateTime(row["FECHA_FIN"])).ToString("dd/MM/yyyy");
                    string anticipo = row["SIMBOLO"].ToString() + " " + row["MONTO_ANTICIPO"].ToString();

                    l_from = l_dt_emailFormat.Rows[0]["from"].ToString();
                    l_subject = l_dt_emailFormat.Rows[0]["subject"].ToString();
                    l_template = l_dt_emailFormat.Rows[0]["body"].ToString();
                    l_scheme = l_dt_emailFormat.Rows[0]["scheme"].ToString();
                    l_style = l_dt_emailFormat.Rows[0]["Style"].ToString();
                    l_head = l_dt_emailFormat.Rows[0]["Head"].ToString();

                    l_template = l_template.Replace("{invoice_nro_auvi}", nro_auvi)
                        .Replace("{invoice_nombres}", nombres)
                        .Replace("{invoice_destino}", destino)
                        .Replace("{invoice_f_ini}", f_ini)
                        .Replace("{invoice_f_fin}", f_fin)
                        .Replace("{invoice_anticipo}", anticipo);

                    l_htmlContent = l_scheme.Replace("{tag_style}", l_style)
                            .Replace("{tag_head}", l_head).Replace("{tag_body}", l_template).Replace("{tag_foot}", l_foot);

                    var sgmessage = new SendGridMessage()
                    {
                        From = new EmailAddress(l_from),
                        Subject = l_subject,
                        HtmlContent = l_htmlContent
                    };

                    sgmessage.AddTo("lmar11xd@gmail.com");
                    SendGrid_Client.SendEmailAsync(sgmessage);
                    g_list_send.Clear();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void SendEmailAutViaje(List<ItemMotivo> motivos)//int[] av_ids, string[] av_aprb
        {
            SendGridClient SendGrid_Client = null;
            DataTable l_dt_emailFormat = null,
                l_dt_emailFormat_r = null,
                l_dt_email_info = null,
                l_dt_av_ids = null;

            string l_SendGridKey,
                l_htmlContent,
                l_from,
                l_subject,
                l_formatId,
                l_formatId_r,
                l_template,
                l_scheme,
                l_style,
                l_head,
                l_foot;

            try
            {
                l_formatId = Dao.GetParameterByKey("FORMATO_EMAIL_AUT_VIAJE_APROBADO");
                l_dt_emailFormat = Dao.GetEmailFormatById(Convert.ToDecimal(l_formatId));

                l_formatId_r = Dao.GetParameterByKey("FORMATO_EMAIL_AUT_VIAJE_RECHAZADO");
                l_dt_emailFormat_r = Dao.GetEmailFormatById(Convert.ToDecimal(l_formatId_r));

                l_SendGridKey = System.Configuration.ConfigurationManager.AppSettings.Get("SendGrid_ApiKey");

                l_foot = l_dt_emailFormat.Rows[0]["Foot"].ToString();
                l_dt_email_info = Dao.GetEmailInformation();

                l_foot = l_foot.Replace("{sider_web_url}", l_dt_email_info.Rows[0]["SiderperuWebUrl"].ToString())
                    .Replace("{sider_web_text}", l_dt_email_info.Rows[0]["SiderperuWebTexto"].ToString())
                    .Replace("{gerdau_web_url}", l_dt_email_info.Rows[0]["GerdarWebUrl"].ToString())
                    .Replace("{gerdau_web_text}", l_dt_email_info.Rows[0]["GerdarWebTexto"].ToString())
                    .Replace("{app_name}", l_dt_email_info.Rows[0]["VisitaAppName"].ToString())
                    .Replace("{sider_short_name}", l_dt_email_info.Rows[0]["SiderperuShortName"].ToString())
                    .Replace("{visita_phone}", l_dt_email_info.Rows[0]["VisitaPhone"].ToString())
                    .Replace("{visita_anexo}", l_dt_email_info.Rows[0]["VisitaAnexo"].ToString())
                    .Replace("{visita_rpc}", l_dt_email_info.Rows[0]["VisitaRpc"].ToString())
                    .Replace("{label_1}", l_dt_email_info.Rows[0]["VisitaLabel1"].ToString());

                SendGrid_Client = new SendGridClient(l_SendGridKey);

                DataRow l_row;
                l_dt_av_ids = new DataTable();
                l_dt_av_ids.Columns.Add("id");
                foreach(var m in motivos)
                {
                    l_row = l_dt_av_ids.NewRow();
                    l_row["id"] = m.CodiAuvi;
                    l_dt_av_ids.Rows.Add(l_row);
                    l_row = null;
                }

                DataTable l_dt_av = Dao.GetAutViajesUpdate(l_dt_av_ids);
                int count_aprb = 0;

                foreach (DataRow row in l_dt_av.Rows)
                {
                    string nombres = row["nombres"].ToString();
                    string destino = row["destino"].ToString();
                    string f_ini = (Convert.IsDBNull(row["f_ini"]) ? default(DateTime) : Convert.ToDateTime(row["f_ini"])).ToString("dd/MM/yyyy");
                    string f_fin = (Convert.IsDBNull(row["f_fin"]) ? default(DateTime) : Convert.ToDateTime(row["f_fin"])).ToString("dd/MM/yyyy");
                    string motivo = row["motivo"].ToString();
                    string estado = row["estado"].ToString();
                    string email = row["email"].ToString();
                    string[] email_split = email.Split(';');
                    string motivo_aprb = motivos[count_aprb].Motivo;
                    count_aprb++;

                    if (email != "")
                    {
                        if (estado == "AG" || estado == "AA")
                        {
                            l_from = l_dt_emailFormat.Rows[0]["from"].ToString();
                            l_subject = l_dt_emailFormat.Rows[0]["subject"].ToString();
                            l_template = l_dt_emailFormat.Rows[0]["body"].ToString();
                            l_scheme = l_dt_emailFormat.Rows[0]["scheme"].ToString();
                            l_style = l_dt_emailFormat.Rows[0]["Style"].ToString();
                            l_head = l_dt_emailFormat.Rows[0]["Head"].ToString();
                            l_template = l_template.Replace("{invoice_nombres}", nombres).Replace("{invoice_destino}", destino).Replace("{invoice_f_ini}", f_ini).Replace("{invoice_f_fin}", f_fin).Replace("{invoice_motivo}", motivo_aprb);
                        }
                        else
                        {
                            l_from = l_dt_emailFormat_r.Rows[0]["from"].ToString();
                            l_subject = l_dt_emailFormat_r.Rows[0]["subject"].ToString();
                            l_template = l_dt_emailFormat_r.Rows[0]["body"].ToString();
                            l_scheme = l_dt_emailFormat_r.Rows[0]["scheme"].ToString();
                            l_style = l_dt_emailFormat_r.Rows[0]["Style"].ToString();
                            l_head = l_dt_emailFormat_r.Rows[0]["Head"].ToString();
                            l_template = l_template.Replace("{invoice_nombres}", nombres).Replace("{invoice_destino}", destino).Replace("{invoice_f_ini}", f_ini).Replace("{invoice_f_fin}", f_fin).Replace("{invoice_motivo}", motivo);
                        }

                        l_htmlContent = l_scheme.Replace("{tag_style}", l_style)
                            .Replace("{tag_head}", l_head).Replace("{tag_body}", l_template).Replace("{tag_foot}", l_foot);

                        var sgmessage = new SendGridMessage()
                        {
                            From = new EmailAddress(l_from),
                            Subject = l_subject,
                            HtmlContent = l_htmlContent
                        };

                        for (int j = 0; j < email_split.Length; j++)
                        {
                            if (email_split[j] != "" && email_split[j] != email)
                            {
                                g_list_send.Add(new EmailAddress(email_split[j]));
                            }
                        }

                        sgmessage.AddTo(email);
                        sgmessage.AddCcs(g_list_send);
                        SendGrid_Client.SendEmailAsync(sgmessage);
                        g_list_send.Clear();
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}