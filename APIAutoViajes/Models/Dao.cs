﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace APIAutoViajes.Models
{
    public class Dao
    {
        public static bool ValidateAccessTester(LoginRequest login)
        {
            int l_value = 0;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("Elearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Validar_CursoWebApi_Tester", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pv_username", login.Username));
                    cmd.Parameters.Add(new SqlParameter("@pv_password", login.Password));
                    cmd.Parameters.Add("@pn_status", SqlDbType.TinyInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToInt32(cmd.Parameters["@pn_status"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value == 1;
        }

        public static DataTable ListTrabajadorExternos(ParmListUsuario parm)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_List_Usuario_App";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo_usuario", parm.TipoUsuario));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_profile_id", parm.ProfileId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_buscar", parm.Buscar));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable ListCentroCosto(ParmBuscar p_buscar)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.sp_viaj_list_planilla_centro_costo_dropdown_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_buscar", p_buscar.Buscar));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable ListDestino(ParmDestino p_buscar)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaje_Destino_List_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pais", p_buscar.CodiPais));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable ListTipoDocumento()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaje_Tipo_Documento_List_App";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable ListSede()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaje_Sede_Trabajo_List_App";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable ListRutaComun(ParmRutaComun p_buscar)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaje_Ruta_Comun_List_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_mod_viaje", p_buscar.ModoViaje));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable ListComedor(ParmComedor parm)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_List_Comedor_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pi_codi_Sede", parm.CodiSede));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable ListTipoServicio()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Tipo_Servicio_List_App";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable ListPais(ParmPais p_tipo)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaje_Pais_List_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo_viaje", p_tipo.Tipo));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable ListPep()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.sp_viaj_list_planilla_pep_dropdown_App";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable ListPepDetalle(ParmPepDetalle param)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.sp_viaj_list_planilla_pep_detalle_dropdown_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pep", param.CodiPep));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable GetListAV(ParamListAV param)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaje_List_Autorizacion_Viaje_App";//Sp_Viaje_List_Autorizacion_Viaje2_App
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", param.Estado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pda_fecha_ini", param.FechaIni));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pda_fecha_fin", param.FechaFin));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pi_codi_perfil", param.CodiPerfil));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pi_codi_pers_aut", param.CodiPersAut));
                    //da.SelectCommand.Parameters.Add(new SqlParameter("@pv_user_name", param.Username));
                    //da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo", param.Tipo));
                    //da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nro_av", param.NroAV));
                    //da.SelectCommand.Parameters.Add(new SqlParameter("@pi_tipo_doc", param.CodiTDoc));
                    //da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nro_doc", param.NumDoc));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable GetListAprobarAV(ParmListApAv param)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaje_AprobarAV_List_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo", param.Tipo));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", param.Estado));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pi_tipo_doc", param.CodiTDoc));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nro_doc", param.NumDoc));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pda_fecha_ini", param.FechaIni));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pda_fecha_fin", param.FechaFin));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pi_codi_perfil", param.CodiPerfil));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pi_codi_pers_aut", param.CodiPersAut));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable GetUltimosViajes(ParmCodiAuvi param)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Sel_gasto_3_ultimo_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_auvi", param.CodiAuvi));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable GetValidaUsuario(Login_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Segu_Validar_Acceso_SP_App";//Sp_Segu_Validar_Acceso_SP_App -- Sp_Segu_Validar_Acceso_App
                    da.SelectCommand.Parameters.Add(new SqlParameter("@UserId", p_Model.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@AppId", p_Model.AppId));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable GetProfileList(Login_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Segu_Usuario_PerfilSistemas";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodeUsuario", p_Model.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodeSistema", p_Model.AppId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        
        public static DataTable InsertarAV(ParamInsertAV p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Ins_Auto_Viaje_APP";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_pers", p_Model.CodiPers));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ficha_sap", p_Model.FichaSap));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_ceco", p_Model.CodiCeco));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_sub_pep", p_Model.CodiSubPep));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_num_contacto", p_Model.NumContacto));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo_viaje", p_Model.TipoViaje));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo_persona", p_Model.TipoPersona));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_fecha_inicio", p_Model.FechaInicio));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_fecha_fin", p_Model.FechaFin));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_cierre_auto", p_Model.CierreAuto));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_dest", p_Model.CodiDest));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_otro_dest", p_Model.OtroDestino));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_mone", p_Model.CodiMone));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_monto", p_Model.Monto));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_justificacion", p_Model.Justificacion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_existe_hospedaje", p_Model.ExisteHospedaje));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pi_codi_pers_aut", p_Model.CodiPersAut));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_fuera_fecha", p_Model.FueraFecha));
                    
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pv_table_itinerario",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtItinerario
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pv_table_comedor",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtComedor
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pv_table_hospedaje",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtHospedaje
                    });
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable InsertarPersona(ParamInsertPersona p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_New_Persona_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ape_paterno", p_Model.ApePaterno));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ape_materno", p_Model.ApeMaterno));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nombres", p_Model.Nombres));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo_doc", p_Model.CodiTDoc));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_num_doc", p_Model.NumDoc));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_celular", p_Model.Celular));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable InsertarPlanilla(ParamInsertPlanilla p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Ins_Planilla_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_plan", p_Model.CodiPlan));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_auvi", p_Model.CodiAuvi));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_ceco", p_Model.CodiCeco));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_fecha_adelanto", p_Model.FechaAdelanto));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_motivo", p_Model.Motivo));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_perfil_id", p_Model.CodiPerfil));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pers", p_Model.CodiPers));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_cierre_planilla", p_Model.CierrePlanilla));                    
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_table_sustento",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtSustento
                    });
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable VerificarDiasHabiles(ParmDiasHabiles p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Verificar_Dias_Habiles_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@fecha_inicio", p_Model.FechaInicio));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@tipo", p_Model.Tipo));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable VerificarDocumento(ParmDocumento p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Verificar_Dni_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo_doc", p_Model.CodiTDoc));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_num_doc", p_Model.NumDoc));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable VerificarSustento(ParmSustento p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Verificar_Sustento_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_pers", p_Model.CodiPers));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable RecuperarAV(ParmCodiAuvi p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Obt_Aut_Viaje_ById_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_auvi", p_Model.CodiAuvi));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable EditarAV(ParamInsertAV p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Edit_Auto_Viaje_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_auvi", p_Model.CodiAuvi));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_pers", p_Model.CodiPers));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ficha_sap", p_Model.FichaSap));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_ceco", p_Model.CodiCeco));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_sub_pep", p_Model.CodiSubPep));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_num_contacto", p_Model.NumContacto));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo_viaje", p_Model.TipoViaje));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo_persona", p_Model.TipoPersona));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_fecha_inicio", p_Model.FechaInicio));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_fecha_fin", p_Model.FechaFin));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_cierre_auto", p_Model.CierreAuto));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_dest", p_Model.CodiDest));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_otro_dest", p_Model.OtroDestino));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_mone", p_Model.CodiMone));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_monto", p_Model.Monto));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_justificacion", p_Model.Justificacion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_existe_hospedaje", p_Model.ExisteHospedaje));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pi_codi_pers_aut", p_Model.CodiPersAut));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_fuera_fecha", p_Model.FueraFecha));

                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pv_table_itinerario",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtItinerario
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pv_table_comedor",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtComedor
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pv_table_hospedaje",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtHospedaje
                    });
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable GetListItinerario(ParmCodiAuvi p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Obt_Itinerario_App";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_auvi", p_Model.CodiAuvi));
                    da.Fill(l_dt);
                }
            }
            catch(Exception e)
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable GetListHospedaje(ParmCodiAuvi p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Obt_Hospedaje_App";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_auvi", p_Model.CodiAuvi));
                    da.Fill(l_dt);
                }
            }
            catch(Exception e)
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable GetListComedor(ParmCodiAuvi p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Obt_Comedores_App";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_auvi", p_Model.CodiAuvi));
                    da.Fill(l_dt);
                }
            }
            catch(Exception e)
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable BorrarAV(ParmCodiAuvi p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Del_AutViaje_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_auvi", p_Model.CodiAuvi));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable AprobarAV(ParamAprobarAV p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Ins_Aprobacion_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Model.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Model.IP));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pv_table_aprobacion",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtMotivos
                    });

                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return l_dt;
        }

        #region Email
        public static string GetParameterByKey(string p_key)
        {
            string l_value = "";
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "dbo.Sp_Viaj_Obt_Parametro_Clave";
                    cmd.Parameters.Add(new SqlParameter("@Clave", p_key));
                    cmd.Parameters.Add("@Dato", SqlDbType.NVarChar, 500).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToString(cmd.Parameters["@Dato"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public static DataTable GetEmailFormatById(decimal p_FormatId)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Sel_Formato_Correo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_fcor", p_FormatId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public static DataTable GetEmailInformation()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Correo_Informacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable GetAutViajesUpdate(DataTable l_dt_av_ids)
        {
            SqlConnection l_cn = null;
            SqlDataAdapter l_da = null;
            DataTable l_dt = null;
            try
            {
                l_cn = Helpers.ConnectToSql("AZURE_ADM");
                if (l_cn != null)
                {
                    l_cn.Open();
                    l_dt = new DataTable();
                    l_da = new SqlDataAdapter();
                    l_da.SelectCommand = l_cn.CreateCommand();
                    l_da.SelectCommand.CommandText = "dbo.Sp_Viaj_Obt_Aut_Viajes";
                    l_da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    l_da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pv_table_av_id",
                        SqlDbType = SqlDbType.Structured,
                        Value = l_dt_av_ids
                    });
                    l_da.Fill(l_dt);
                }
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                if (l_cn != null)
                {
                    l_cn.Close();
                    l_cn.Dispose();
                }
            }
            return l_dt;
        }
        #endregion

        public static DataTable GetPlanilla(ParmCodiAuvi p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaje_List_Planilla_App";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_auvi", p_Model.CodiAuvi));
                    da.Fill(l_dt);
                }
            }
            catch(Exception ex)
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable GetComprobantes(Int32 p_CodiPlan)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaje_List_Sustento_App";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_plan", p_CodiPlan));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable GetPlanillaMotivo()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.sp_viaj_list_planilla_motivo_dropdown_App";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable GetPlanillaTipoDoc()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.sp_viaj_list_planilla_tipo_documento_dropdown_App";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable GetTipoComprobante(ParmCodiTDoc p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.sp_viaj_obt_planilla_tipo_doc_App";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_tdoc", p_Model.CodiTDoc));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable GetUserInfo(Login_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Get_User_Info";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_username", p_Model.Username));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public static DataTable GetAutViajeEmail(int codi_auvi)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("AZURE_ADM"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Viaj_Obt_Aut_Viaje_Email";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_auvi", codi_auvi));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
    }
}