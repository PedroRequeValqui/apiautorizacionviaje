﻿using System;
using System.Collections.Generic;
using System.Data;

namespace APIAutoViajes.Models
{
    public class Response
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public int Other { get; set; }
        public string Exception { get; set; }

    }

    public class ParamKey
    {
        public string Key { get; set; }

    }

    public class ResultListUsuario
    {
        public int CodiPers { get; set; }
        public string Nombre { get; set; }
        public string NroDoc { get; set; }
        public string FichaSap { get; set; }
        public string Celular { get; set; }
        public string CodiCeco { get; set; }
        public string CecoDescripcion{ get; set; }
    }

    public class ParmListUsuario
    {
        public string TipoUsuario { get; set; }
        public decimal ProfileId { get; set; }
        public string Buscar { get; set; }
    }

    public class ParamListAV
    {
        public string Tipo { get; set; }
        public string NroAV { get; set; }
        public string Estado { get; set; }
        public int CodiTDoc { get; set; }
        public string NumDoc { get; set; }
        public string FechaIni { get; set; }
        public string FechaFin { get; set; }
        public int CodiPerfil { get; set; }
        public string Username { get; set; }
        public int CodiPersAut { get; set; }
    }

    public class ResultListAV
    {
        public int CodiAuvi { get; set; }
        public int NroAuvi { get; set; }
        public string TipoPersona { get; set; }
        public string Nombres { get; set; }
        public string Fechas { get; set; }
        public string Destino { get; set; }
        public string Justificacion { get; set; }
        public string Anticipo { get; set; }
        public string Estado { get; set; }
    }

    public class ResultListCentroCosto
    {
        public string CodiCeco { get; set; }
        public string Nombre { get; set; }
    }

    public class ResultLogin
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public int CodiPers { get; set; }
        public string FichaSap { get; set; }
        public DataTable detalle { get; set; }
    }
    public class Login_Model
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public decimal AppId { get; set; }
    }

    public class ParmBuscar
    {
        public string Buscar { get; set; }
    }

    public class ParmDestino
    {
        public string CodiPais { get; set; }
    }

    public class ResultListDestino
    {
        public int CodiDestino { get; set; }
        public string Ciudad { get; set; }
    }

    public class ResultListTipoDocumento
    {
        public string CodiTdoc { get; set; }
        public string Nombre { get; set; }
    }
    public class ResultListSede
    {
        public string CodiSede { get; set; }
        public string Nombre { get; set; }
    }

    public class ParmRutaComun
    {
        public string ModoViaje { get; set; }
    }

    public class ResultListRutaComun
    {
        public int CodiRuco { get; set; }
        public string Ruta { get; set; }
        public string Origen { get; set; }
        public string Destino { get; set; }
        public decimal NroHoras { get; set; }
        public string CodiDest { get; set; }
    }

    public class ParmComedor
    {
        public decimal CodiSede { get; set; }
    }

    public class ResultListComedor
    {
        public string CodiCome { get; set; }
        public string Nombre { get; set; }
    }

    public class ResultListTipoServicio
    {
        public string CodiTise { get; set; }
        public string Nombre { get; set; }
        public string HoraIni { get; set; }
        public string HoraFin { get; set; }
    }

    public class ParmPais
    {
        public string Tipo { get; set; }
    }

    public class ResultListPais
    {
        public string CodiPais { get; set; }
        public string Nombre { get; set; }
    }

    public class ResultListPEP
    {
        public string CodiPep { get; set; }
        public string Descripcion { get; set; }
    }

    public class ParmPepDetalle
    {
        public decimal CodiPep { get; set; }
    }

    public class ResultListPEPDetalle
    {
        public string CodiSubPEP { get; set; }
        public string Descripcion { get; set; }
    }

    public class InsertAutorizacionViaje
    {
        public int CodiSubPEP { get; set; }
        public string Descripcion { get; set; }
        public List<tblDetalle_Autorizacion> detalle { get; set; }
        public DataTable DtAutViajeDetalle { get; set; }
    }

    public class tblDetalle_Autorizacion
    {
        public int Id { get; set; }
        public string detalle { get; set; }
    }

    public class ParamInsertAV
    {
        public int CodiAuvi { get; set; }
        public int CodiPers { get; set; }
        public string FichaSap { get; set; }
        public string CodiCeco { get; set; }
        public int CodiSubPep { get; set; }
        public string NumContacto { get; set; }
        public string TipoViaje { get; set; }
        public string TipoPersona { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public string CierreAuto { get; set; }
        public int CodiDest { get; set; }
        public string OtroDestino { get; set; }
        public int CodiMone { get; set; }
        public decimal Monto { get; set; }
        public string Justificacion { get; set; }
        public string ExisteHospedaje { get; set; }
        public int CodiPersAut { get; set; }
        public string FueraFecha { get; set; }

        public List<AVItinerario> ListItinerario { get; set; }
        public List<AVHospedaje> ListHospedaje { get; set; }
        public List<AVComedor> ListComedor { get; set; }

        public DataTable DtItinerario { get; set; }
        public DataTable DtHospedaje { get; set; }
        public DataTable DtComedor { get; set; }
    }

    public class AVItinerario
    {
        public int CodiAuit { get; set; }
        public string CodiVia { get; set; }
        public int CodiRuta { get; set; }
        public string Origen { get; set; }
        public string Destino { get; set; }
        public string FechaHora { get; set; }
    }

    public class AVHospedaje
    {
        public int CodiAuho { get; set; }
        public int CodiDest { get; set; }
        public string FInicio { get; set; }
        public string FFin { get; set; }
    }

    public class AVComedor
    {
        public int CodiAuco { get; set; }
        public string FDesde { get; set; }
        public string FHasta { get; set; }
        public int CodiCome { get; set; }
        public int CodiTise { get; set; }
    }

    public class ParamInsertPersona
    {
        public int CodiTDoc { get; set; }
        public string NumDoc { get; set; }
        public string ApePaterno { get; set; }
        public string ApeMaterno { get; set; }
        public string Nombres { get; set; }
        public string Celular { get; set; }
    }

    public class ParmDiasHabiles
    {
        public string FechaInicio { get; set; }
        public string Tipo { get; set; }
    }

    public class ParmDocumento
    {
        public int CodiTDoc { get; set; }
        public string NumDoc { get; set; }
    }

    public class ParmSustento
    {
        public int CodiPers { get; set; }
    }

    public class ParmCodiAuvi
    {
        public int CodiAuvi { get; set; }
    }
    public class ResultRecuperarAV
    {
        public int CodiAuvi { get; set; }
        public int NroAuvi { get; set; }
        public string TipoPersona { get; set; }
        public int CodiPers { get; set; }
        public string Nombres { get; set; }
        public int CodiTDoc { get; set; }
        public string NumDoc { get; set; }
        public string Celular { get; set; }
        public string FichaSap { get; set; }
        public int CodiTrab { get; set; }
        public string CodiCeco { get; set; }
        public string Ceco { get; set; }
        public int CodiPep { get; set; }
        public string Pep { get; set; }
        public int CodiSubPep { get; set; }
        public string SubPep { get; set; }
        public string FechaIni { get; set; }
        public string FechaFin { get; set; }
        public string TipoViaje { get; set; }
        public int CodiPais { get; set; }
        public string Pais { get; set; }
        public int CodiDest { get; set; }
        public string Destino { get; set; }
        public string OtroDestino { get; set; }
        public string FlagCierre { get; set; }
        public int CodiMone { get; set; }
        public string Moneda { get; set; }
        public string Monto { get; set; }
        public string Justificacion { get; set; }
        public string FueraFecha { get; set; }

        public DataTable ListItinerario { get; set; }
        public DataTable ListHospedaje { get; set; }
        public DataTable ListComedor { get; set; }
    }
    public class ParmListApAv
    {
        public string Tipo { get; set; }
        public string Estado { get; set; }
        public int CodiTDoc { get; set; }
        public string NumDoc { get; set; }
        public string FechaIni { get; set; }
        public string FechaFin { get; set; }
        public int CodiPerfil { get; set; }
        public int CodiPersAut { get; set; }
    }
    public class ResultListAprobarAV
    {
        public int CodiAuvi { get; set; }
        public int NroAuvi { get; set; }
        public string TipoPersona { get; set; }
        public string Nombres { get; set; }
        public string Destino { get; set; }
        public string Fechas { get; set; }
        public string Monto { get; set; }
        public string Justificacion { get; set; }
        public string Estado { get; set; }
        public string FueraFecha { get; set; }
        public string Rechazo { get; set; }
    }

    public class ResultUltimosViajes
    {
        public int NroAuvi { get; set; }
        public string TipoViaje { get; set; }
        public string Destino { get; set; }
        public string Fechas { get; set; }
        public string Monto { get; set; }
        public string Justificacion { get; set; }
    }

    public class ParamAprobarAV
    {
        public string Username { get; set; }
        public string IP { get; set; }
        public List<ItemMotivo> Motivos { get; set; }
        public DataTable DtMotivos { get; set; }
    }
    public class ItemMotivo
    {
        public int CodiAuvi { get; set; }
        public string Motivo { get; set; }
        public string Estado { get; set; }
    }

    public class ParamInsertPlanilla
    {
        public int CodiPlan { get; set; }
        public int CodiAuvi { get; set; }
        public string CodiCeco { get; set; }
        public string FechaAdelanto { get; set; }
        public string Motivo { get; set; }
        public string CierrePlanilla { get; set; }
        public int CodiPers { get; set; }
        public int CodiPerfil { get; set; }
        public List<Sustento> ListSustento { get; set; }
        public DataTable DtSustento { get; set; }
    }

    public class Sustento
    {
        public int CodiSust { get; set; }
        public int CodiTDoct { get; set; }
        public string FechaDoc { get; set; }
        public int CodiMotivo { get; set; }
        public string NumDoc { get; set; }
        public string Descripcion { get; set; }
        public decimal Monto { get; set; }
    }

    public class ResultPlanilla
    {
        public int CodiPlan { get; set; }
        public int CodiAuvi { get; set; }
        public int NroPlan { get; set; }
        public string FichaSap { get; set; }
        public string Nombre { get; set; }
        public string CodiCeco { get; set; }
        public string Ceco { get; set; }
        public string Simbolo { get; set; }
        public string FechaAdelanto { get; set; }
        public string FechaRegistro { get; set; }
        public string Motivo { get; set; }
        public string Destino { get; set; }
        public decimal Adelanto { get; set; }
        public string Estado { get; set; }

        public DataTable ListComprobante { get; set; }
    }
    
    public class ResultMotivo
    {
        public int CodiMotivo { get; set; }
        public string Descripcion { get; set; }
    }
    public class ResultTipoDoc
    {
        public int CodiTdoc { get; set; }
        public string Descripcion { get; set; }
    }

    public class ParmCodiTDoc
    {
        public int CodiTDoc { get; set; }
    }

    public class ResultUserInfo
    {
        public string Nombres { get; set; }
        public string FichaSap { get; set; }
        public string Telefono { get; set; }
        public string Ceco { get; set; }
        public string Area { get; set; }
        public string SubArea { get; set; }
    }
}