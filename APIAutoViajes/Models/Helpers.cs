﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace APIAutoViajes.Models
{
    public class Helpers
    {
        public static SqlConnection ConnectToSql(string ps_name)
        {
            string ls_connection = System.Configuration.ConfigurationManager.ConnectionStrings[ps_name].ConnectionString;
            return new SqlConnection(ls_connection);
        }
        public static Response GetMessageCrud(DataTable p_dt)
        {
            string l_message = null;
            int l_code = 0;
            try
            {
                foreach (DataRow row in p_dt.Rows)
                {
                    l_message = row["Mensaje"].ToString();
                    l_code = Convert.ToInt32(row["Value"]);
                }
                return new Response() { Status = l_code, Message = l_message };
            }
            catch
            {
                throw;
            }
        }
    
        public static Response GetException(Exception p_e)
        {
            string l_message = p_e.Message;
            string l_exception = "An exception occurred.\n" +
                "- Type : " + p_e.GetType().Name + "\n" +
                "- Message: " + p_e.Message + "\n" +
                "- Stack Trace: " + p_e.StackTrace;
            Exception l_ie = p_e.InnerException;
            if (l_ie != null)
            {
                l_exception += "\n";
                l_exception += "The Inner Exception:\n" +
                    "- Exception Name: " + l_ie.GetType().Name + "\n" +
                    "- Message: " + l_ie.Message + "\n" +
                    "- Stack Trace: " + l_ie.StackTrace;
            }
            return new Response() { Status = -1, Message = l_message, Exception = l_exception };
        }

        public static List<ResultListUsuario> GetListTrabajadorExterno(ParmListUsuario parm)
        {
            ResultListUsuario p_datos = null;
            List<ResultListUsuario> _employees = new List<ResultListUsuario>();
            var _result = Dao.ListTrabajadorExternos(parm);
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultListUsuario();
                    p_datos.CodiPers = Convert.ToInt32(row["CODI_PERS"]);
                    p_datos.Nombre = Convert.ToString(row["NOMBRE"]);
                    p_datos.NroDoc = Convert.ToString(row["NRO_DOC"]);
                    p_datos.FichaSap = Convert.ToString(row["FICHA_SAP"]);
                    p_datos.Celular = Convert.ToString(row["CELULAR"]);
                    p_datos.CodiCeco = Convert.ToString(row["CODI_CECO"]);
                    p_datos.CecoDescripcion = Convert.ToString(row["CECO_DESCRIPCION"]);
                    _employees.Add(p_datos);
                }
                return _employees;
            }
            catch
            {
                throw;
            }
        }

        public static List<ResultListCentroCosto> GetListCentroCosto(ParmBuscar p_buscar)
        {
            ResultListCentroCosto p_datos = null;
            List<ResultListCentroCosto> _list = new List<ResultListCentroCosto>();
            var _result = Dao.ListCentroCosto(p_buscar);
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultListCentroCosto();
                    p_datos.CodiCeco = Convert.ToString(row["CODI_CECO"]);
                    p_datos.Nombre = Convert.ToString(row["DESCRIPCION"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }

        public static List<ResultListDestino> GetListDestino(ParmDestino p_buscar)
        {
            ResultListDestino p_datos = null;
            List<ResultListDestino> _list = new List<ResultListDestino>();
            var _result = Dao.ListDestino(p_buscar);
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultListDestino();
                    p_datos.CodiDestino = Convert.ToInt32(row["CODI_DEST"]);
                    p_datos.Ciudad = Convert.ToString(row["CIUDAD"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }
        public static List<ResultListTipoDocumento> GetListTipoDocumento()
        {
            ResultListTipoDocumento p_datos = null;
            List<ResultListTipoDocumento> _list = new List<ResultListTipoDocumento>();
            var _result = Dao.ListTipoDocumento();
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultListTipoDocumento();
                    p_datos.CodiTdoc = Convert.ToString(row["CODI_TDOC"]);
                    p_datos.Nombre = Convert.ToString(row["DESCRIPCION_CORTA"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }

        public static List<ResultListSede> GetListSede()
        {
            ResultListSede p_datos = null;
            List<ResultListSede> _list = new List<ResultListSede>();
            var _result = Dao.ListSede();
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultListSede();
                    p_datos.CodiSede = Convert.ToString(row["CODI_SEDE"]);
                    p_datos.Nombre = Convert.ToString(row["DESCRIPCION"]);
                    _list.Add(p_datos);
                }
                //foreach (DataRow row in _result.Rows)
                //{
                //    _list.Add(new List<object>
                //    {
                //        row["CODI_SEDE"],//1
                //        row["DESCRIPCION"]//2
                //    });
                //}
                return _list;
            }
            catch
            {
                throw;
            }
        }
        public static List<ResultListRutaComun> GetListRutaComun(ParmRutaComun p_buscar)
        {
            ResultListRutaComun p_datos = null;
            List<ResultListRutaComun> _list = new List<ResultListRutaComun>();
            var _result = Dao.ListRutaComun(p_buscar);
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultListRutaComun();
                    p_datos.CodiRuco = Convert.ToInt32(row["CODI_RUCO"]);
                    p_datos.Ruta = Convert.ToString(row["RUTA"]);
                    p_datos.Origen = Convert.ToString(row["ORIGEN"]);
                    p_datos.Destino = Convert.ToString(row["DESTINO"]);
                    p_datos.NroHoras = Convert.ToDecimal(row["NRO_HORAS"]);
                    p_datos.CodiDest = Convert.ToString(row["CODI_DEST"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }
        public static List<ResultListComedor> GetListComedor(ParmComedor param)
        {
            ResultListComedor p_datos = null;
            List<ResultListComedor> _list = new List<ResultListComedor>();
            var _result = Dao.ListComedor(param);
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultListComedor();
                    p_datos.CodiCome = Convert.ToString(row["CODI_COME"]);
                    p_datos.Nombre = Convert.ToString(row["DESC_COME"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }
        public static List<ResultListTipoServicio> GetListTipoServicio()
        {
            ResultListTipoServicio p_datos = null;
            List<ResultListTipoServicio> _list = new List<ResultListTipoServicio>();
            var _result = Dao.ListTipoServicio();
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultListTipoServicio();
                    p_datos.CodiTise = Convert.ToString(row["CODI_TISE"]);
                    p_datos.Nombre = Convert.ToString(row["DESC_TIPO_SERV"]);
                    p_datos.HoraIni = Convert.ToString(row["HORA_INI"]);
                    p_datos.HoraFin = Convert.ToString(row["HORA_FIN"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }
        public static List<ResultListPais> GetListPais(ParmPais p_tipo)
        {
            ResultListPais p_datos = null;
            List<ResultListPais> _list = new List<ResultListPais>();
            var _result = Dao.ListPais(p_tipo);
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultListPais();
                    p_datos.CodiPais = Convert.ToString(row["CODI_PAIS"]);
                    p_datos.Nombre = Convert.ToString(row["DESCRIPCION"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }

        public static List<ResultListPEP> GetListPep()
        {
            ResultListPEP p_datos = null;
            List<ResultListPEP> _list = new List<ResultListPEP>();
            var _result = Dao.ListPep();
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultListPEP();
                    p_datos.CodiPep = Convert.ToString(row["CODI_PEP"]);
                    p_datos.Descripcion = Convert.ToString(row["DESCRIPCION"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }
        public static List<ResultListPEPDetalle> GetListPepDetalle(ParmPepDetalle param)
        {
            ResultListPEPDetalle p_datos = null;
            List<ResultListPEPDetalle> _list = new List<ResultListPEPDetalle>();
            var _result = Dao.ListPepDetalle(param);
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultListPEPDetalle();
                    p_datos.CodiSubPEP = Convert.ToString(row["CODI_SUB_PEP"]);
                    p_datos.Descripcion = Convert.ToString(row["DESCRIPCION"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }

        public static List<ResultListAV> GetListAV(ParamListAV param)
        {
            ResultListAV p_datos = null;
            List<ResultListAV> _list = new List<ResultListAV>();
            var _result = Dao.GetListAV(param);
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultListAV();
                    p_datos.CodiAuvi = Convert.ToInt32(row["CODI_AUVI"]);
                    p_datos.NroAuvi = Convert.ToInt32(row["NRO_AUTORIZACION"]);
                    p_datos.TipoPersona = Convert.ToString(row["TIPO_PERSONA"]);
                    p_datos.Nombres = Convert.ToString(row["NOMBRES"]);
                    p_datos.Destino = Convert.ToString(row["DESTINO"]);
                    p_datos.Fechas = Convert.ToString(row["FECHAS"]);
                    p_datos.Justificacion = Convert.ToString(row["JUSTIFICACION"]);
                    p_datos.Anticipo = Convert.ToString(row["ANTICIPO"]);
                    p_datos.Estado = Convert.ToString(row["ESTADO_AV"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }

        public static List<ResultListAprobarAV> GetListAprobarAV(ParmListApAv param)
        {
            ResultListAprobarAV p_datos = null;
            List<ResultListAprobarAV> _list = new List<ResultListAprobarAV>();
            var _result = Dao.GetListAprobarAV(param);
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultListAprobarAV();
                    p_datos.CodiAuvi = Convert.ToInt32(row["CODI_AUVI"]);
                    p_datos.NroAuvi = Convert.ToInt32(row["NRO_AUVI"]);
                    p_datos.TipoPersona = Convert.ToString(row["TIPO_PERSONA"]);
                    p_datos.Destino = Convert.ToString(row["DESTINO"]);
                    p_datos.Nombres = Convert.ToString(row["NOMBRES"]);
                    p_datos.Fechas = Convert.ToString(row["FECHAS"]);
                    p_datos.Monto = Convert.ToString(row["MONTO"]);
                    p_datos.Justificacion = Convert.ToString(row["JUSTIFICACION"]);
                    p_datos.Estado = Convert.ToString(row["ESTADO_AV"]);
                    p_datos.FueraFecha = Convert.ToString(row["OBS"]);
                    p_datos.Rechazo = Convert.ToString(row["MOTIVO_RECHAZO"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }
        public static List<ResultUltimosViajes> GetUltimosViajes(ParmCodiAuvi param)
        {
            ResultUltimosViajes p_datos = null;
            List<ResultUltimosViajes> _list = new List<ResultUltimosViajes>();
            var _result = Dao.GetUltimosViajes(param);
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultUltimosViajes();
                    p_datos.NroAuvi = Convert.ToInt32(row["NRO_AUTORIZACION"]);
                    p_datos.TipoViaje = Convert.ToString(row["TIPO_VIAJE"]);
                    p_datos.Destino = Convert.ToString(row["DESTINO"]);
                    p_datos.Fechas = Convert.ToString(row["FECHAS"]);
                    p_datos.Monto = Convert.ToString(row["MONTO"]);
                    p_datos.Justificacion = Convert.ToString(row["JUSTIFICACION"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }
        public static DataTable GetProfiles(Login_Model p_Model)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("codi_perf");
            dt.Columns.Add("descripcion");
            
            var l_records = Dao.GetProfileList(p_Model);
            foreach (DataRow row in l_records.Rows)
            {
                var dr = dt.NewRow();
                dr[0] = row["codi_perf"];
                dr[1] = row["descripcion"];
                dt.Rows.Add(dr);
            }
            ClearDataTable(l_records);
            return dt;
        }

        public static ResultLogin GetValidaUsuario(Login_Model p_Model)
        {
            ResultLogin p_datos = null;
            try
            {
                var l_validate = Dao.GetValidaUsuario(p_Model);
                p_datos = new ResultLogin();
                p_datos.Status = l_validate.Rows[0]["Value"].ToString();
                p_datos.Message = l_validate.Rows[0]["Mensaje"].ToString();

                if (l_validate.Rows[0]["Value"].ToString() == "1000")
                {
                    p_datos.CodiPers = Convert.ToInt32(l_validate.Rows[0]["CodiPers"].ToString());
                    p_datos.FichaSap = l_validate.Rows[0]["FichaSap"].ToString();
                    p_datos.detalle = GetProfiles(p_Model);
                }

                ClearDataTable(l_validate);

                return p_datos;
            }
            catch
            {
                throw;
            }
        }
        
        public static Response InsertarAV(ParamInsertAV p_Model)
        {
            Response p_datos = null;
            DataRow l_row;
            try
            {
                p_Model.DtItinerario = new DataTable();
                p_Model.DtItinerario.Columns.Add("pv_mod_viaje");
                p_Model.DtItinerario.Columns.Add("pv_ruta");
                p_Model.DtItinerario.Columns.Add("pv_origen");
                p_Model.DtItinerario.Columns.Add("pv_destino");
                p_Model.DtItinerario.Columns.Add("pv_fecha_sol");

                for (var i = 0; i < p_Model.ListItinerario.Count; i++)
                {
                    l_row = p_Model.DtItinerario.NewRow();
                    l_row["pv_mod_viaje"] = p_Model.ListItinerario[i].CodiVia;
                    l_row["pv_ruta"] = p_Model.ListItinerario[i].CodiRuta;
                    l_row["pv_origen"] = p_Model.ListItinerario[i].Origen;
                    l_row["pv_destino"] = p_Model.ListItinerario[i].Destino;
                    l_row["pv_fecha_sol"] = p_Model.ListItinerario[i].FechaHora;
                    p_Model.DtItinerario.Rows.Add(l_row);
                    l_row = null;
                }

                p_Model.DtHospedaje = new DataTable();
                p_Model.DtHospedaje.Columns.Add("pv_codi_dest");
                p_Model.DtHospedaje.Columns.Add("pv_fecha_inicio_hosp");
                p_Model.DtHospedaje.Columns.Add("pv_fecha_fin_hosp");

                for (var i = 0; i < p_Model.ListHospedaje.Count; i++)
                {
                    l_row = p_Model.DtHospedaje.NewRow();
                    l_row["pv_codi_dest"] = p_Model.ListHospedaje[i].CodiDest;
                    l_row["pv_fecha_inicio_hosp"] = p_Model.ListHospedaje[i].FInicio;
                    l_row["pv_fecha_fin_hosp"] = p_Model.ListHospedaje[i].FFin;
                    p_Model.DtHospedaje.Rows.Add(l_row);
                    l_row = null;
                }

                p_Model.DtComedor = new DataTable();
                p_Model.DtComedor.Columns.Add("pv_fecha_inicio_come");
                p_Model.DtComedor.Columns.Add("pv_fecha_fin_come");
                p_Model.DtComedor.Columns.Add("pv_codi_comedor");
                p_Model.DtComedor.Columns.Add("pv_tipo_servicio");

                for (var i = 0; i < p_Model.ListComedor.Count; i++)
                {
                    l_row = p_Model.DtComedor.NewRow();
                    l_row["pv_fecha_inicio_come"] = p_Model.ListComedor[i].FDesde;
                    l_row["pv_fecha_fin_come"] = p_Model.ListComedor[i].FHasta;
                    l_row["pv_codi_comedor"] = p_Model.ListComedor[i].CodiCome;
                    l_row["pv_tipo_servicio"] = p_Model.ListComedor[i].CodiTise;
                    p_Model.DtComedor.Rows.Add(l_row);
                    l_row = null;
                }

                var l_validate = Dao.InsertarAV(p_Model);

                p_datos = new Response();
                p_datos.Status = Convert.ToInt32(l_validate.Rows[0]["Value"]);
                p_datos.Message = l_validate.Rows[0]["Mensaje"].ToString();
                p_datos.Other = Convert.ToInt32(l_validate.Rows[0]["Other"].ToString());

                try {
                    EmailHelper.SendEmailNuevaAV(p_datos.Other);
                } catch {
                    p_datos.Message = "AV registrada, pero no se pudo enviar el Correo Electrónico para Administración.";
                }

                ClearDataTable(l_validate);

                return p_datos;
            }
            catch
            {
                throw;
            }
        }

        public static Response InsertarPersona(ParamInsertPersona p_Model)
        {
            Response p_datos = null;
            try
            {
                var l_validate = Dao.InsertarPersona(p_Model);
                foreach (DataRow row in l_validate.Rows)
                {
                    p_datos = new Response();
                    p_datos.Status = Convert.ToInt32(l_validate.Rows[0]["Value"]);
                    p_datos.Message = l_validate.Rows[0]["Mensaje"].ToString();
                    p_datos.Other = Convert.ToInt32(l_validate.Rows[0]["Other"].ToString());
                }

                ClearDataTable(l_validate);

                return p_datos;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static Response InsertarPlanilla(ParamInsertPlanilla p_Model)
        {
            Response p_datos = null;
            DataRow l_row;
            try
            {
                p_Model.DtSustento = new DataTable();
                p_Model.DtSustento.Columns.Add("pn_codi_sust");
                p_Model.DtSustento.Columns.Add("pn_codi_tipo_doc");
                p_Model.DtSustento.Columns.Add("pd_fecha_doc");
                p_Model.DtSustento.Columns.Add("pn_codi_motivo");
                p_Model.DtSustento.Columns.Add("pv_nro_doc"); 
                p_Model.DtSustento.Columns.Add("pv_descripcion");
                p_Model.DtSustento.Columns.Add("pn_monto");

                for (var i = 0; i < p_Model.ListSustento.Count; i++)
                {
                    l_row = p_Model.DtSustento.NewRow();
                    l_row["pn_codi_sust"] = p_Model.ListSustento[i].CodiSust;
                    l_row["pn_codi_tipo_doc"] = p_Model.ListSustento[i].CodiTDoct;
                    l_row["pd_fecha_doc"] = p_Model.ListSustento[i].FechaDoc;
                    l_row["pn_codi_motivo"] = p_Model.ListSustento[i].CodiMotivo;
                    l_row["pv_nro_doc"] = p_Model.ListSustento[i].NumDoc;
                    l_row["pv_descripcion"] = p_Model.ListSustento[i].Descripcion;
                    l_row["pn_monto"] = p_Model.ListSustento[i].Monto;
                    p_Model.DtSustento.Rows.Add(l_row);
                    l_row = null;
                }

                var l_validate = Dao.InsertarPlanilla(p_Model);
                foreach (DataRow row in l_validate.Rows)
                {
                    p_datos = new Response();
                    p_datos.Status = Convert.ToInt32(l_validate.Rows[0]["Value"]);
                    p_datos.Message = l_validate.Rows[0]["Mensaje"].ToString();
                    p_datos.Other = Convert.ToInt32(l_validate.Rows[0]["Other"].ToString());
                }

                ClearDataTable(l_validate);

                return p_datos;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static Response VerificarDiasHabiles(ParmDiasHabiles p_Model)
        {
            Response p_datos = null;
            try
            {
                var l_validate = Dao.VerificarDiasHabiles(p_Model);
                foreach (DataRow row in l_validate.Rows)
                {
                    p_datos = new Response();
                    p_datos.Status = Convert.ToInt32(l_validate.Rows[0]["Value"]);
                    p_datos.Message = l_validate.Rows[0]["Mensaje"].ToString();
                }

                ClearDataTable(l_validate);

                return p_datos;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static Response VerificarDocumento(ParmDocumento p_Model)
        {
            Response p_datos = null;
            try
            {
                var l_validate = Dao.VerificarDocumento(p_Model);
                foreach (DataRow row in l_validate.Rows)
                {
                    p_datos = new Response();
                    p_datos.Status = Convert.ToInt32(l_validate.Rows[0]["Value"]);
                    p_datos.Message = l_validate.Rows[0]["Mensaje"].ToString();
                }

                ClearDataTable(l_validate);

                return p_datos;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static Response VerificarSustento(ParmSustento p_Model)
        {
            Response p_datos = null;
            try
            {
                var l_validate = Dao.VerificarSustento(p_Model);
                foreach (DataRow row in l_validate.Rows)
                {
                    p_datos = new Response();
                    p_datos.Status = Convert.ToInt32(l_validate.Rows[0]["Value"]);
                    p_datos.Message = l_validate.Rows[0]["Mensaje"].ToString();
                }

                ClearDataTable(l_validate);

                return p_datos;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static ResultRecuperarAV RecuperarAV(ParmCodiAuvi p_Model)
        {
            ResultRecuperarAV p_datos = null;
            try
            {
                var l_validate = Dao.RecuperarAV(p_Model);
                foreach (DataRow row in l_validate.Rows)
                {
                    p_datos = new ResultRecuperarAV();
                    p_datos.CodiAuvi = Convert.ToInt32(l_validate.Rows[0]["CODI_AUVI"]);
                    p_datos.NroAuvi = Convert.ToInt32(l_validate.Rows[0]["NRO_AUTORIZACION"]);
                    p_datos.TipoPersona = Convert.ToString(l_validate.Rows[0]["TIPO_PERSONA"]);
                    p_datos.CodiPers = Convert.ToInt32(l_validate.Rows[0]["CODI_PERS"]);
                    p_datos.Nombres = Convert.ToString(l_validate.Rows[0]["NOMBRES"]);
                    p_datos.CodiTDoc = Convert.ToInt32(l_validate.Rows[0]["CODI_TDOC"]);
                    p_datos.NumDoc = Convert.ToString(l_validate.Rows[0]["NUM_DOC"]);
                    p_datos.Celular = Convert.ToString(l_validate.Rows[0]["CELULAR"]);
                    p_datos.FichaSap = Convert.ToString(l_validate.Rows[0]["FICHA_SAP"]);
                    p_datos.CodiTrab = Convert.ToInt32(l_validate.Rows[0]["CODI_TRAB"]);

                    p_datos.CodiCeco = Convert.ToString(l_validate.Rows[0]["CODI_CECO"]);
                    p_datos.Ceco = Convert.ToString(l_validate.Rows[0]["CECO"]);
                    p_datos.CodiPep = Convert.ToInt32(l_validate.Rows[0]["CODI_PEP"]);
                    p_datos.Pep = Convert.ToString(l_validate.Rows[0]["PEP"]);
                    p_datos.CodiSubPep = Convert.ToInt32(l_validate.Rows[0]["CODI_SUB_PEP"]);
                    p_datos.SubPep = Convert.ToString(l_validate.Rows[0]["SUB_PEP"]);

                    p_datos.FechaIni = Convert.ToString(l_validate.Rows[0]["FECHA_INICIO"]);
                    p_datos.FechaFin = Convert.ToString(l_validate.Rows[0]["FECHA_FIN"]);
                    p_datos.TipoViaje = Convert.ToString(l_validate.Rows[0]["TIPO_VIAJE"]);
                    p_datos.CodiPais = Convert.ToInt32(l_validate.Rows[0]["CODI_PAIS"]);
                    p_datos.Pais = Convert.ToString(l_validate.Rows[0]["PAIS"]);
                    p_datos.CodiDest = Convert.ToInt32(l_validate.Rows[0]["CODI_DEST"]);
                    p_datos.Destino = Convert.ToString(l_validate.Rows[0]["DESTINO"]);
                    p_datos.OtroDestino = Convert.ToString(l_validate.Rows[0]["OTRO_DESTINO"]);

                    p_datos.FlagCierre = Convert.ToString(l_validate.Rows[0]["FLAG_CIERRE"]);
                    p_datos.CodiMone = Convert.ToInt32(l_validate.Rows[0]["CODI_MONE"]);
                    p_datos.Moneda = Convert.ToString(l_validate.Rows[0]["MONEDA"]);
                    p_datos.Monto = Convert.ToString(l_validate.Rows[0]["MONTO"]);
                    p_datos.Justificacion = Convert.ToString(l_validate.Rows[0]["JUSTIFICACION"]);
                    p_datos.FueraFecha = Convert.ToString(l_validate.Rows[0]["FLAG_FUERA"]);

                    p_datos.ListItinerario = GetListItinerario(p_Model);
                    p_datos.ListHospedaje = GetListHospedaje(p_Model);
                    p_datos.ListComedor = GetListComedor(p_Model);
                }

                ClearDataTable(l_validate);

                return p_datos;
            }
            catch (Exception e)
            {
                throw;
            }
        }
        public static Response EditarAV(ParamInsertAV p_Model)
        {
            Response p_datos = null;
            DataRow l_row;
            try
            {
                p_Model.DtItinerario = new DataTable();
                p_Model.DtItinerario.Columns.Add("pv_codi_auit");
                p_Model.DtItinerario.Columns.Add("pv_mod_viaje");
                p_Model.DtItinerario.Columns.Add("pv_ruta");
                p_Model.DtItinerario.Columns.Add("pv_origen");
                p_Model.DtItinerario.Columns.Add("pv_destino");
                p_Model.DtItinerario.Columns.Add("pv_fecha_sol");

                for (var i = 0; i < p_Model.ListItinerario.Count; i++)
                {
                    l_row = p_Model.DtItinerario.NewRow();
                    l_row["pv_codi_auit"] = p_Model.ListItinerario[i].CodiAuit;
                    l_row["pv_mod_viaje"] = p_Model.ListItinerario[i].CodiVia;
                    l_row["pv_ruta"] = p_Model.ListItinerario[i].CodiRuta;
                    l_row["pv_origen"] = p_Model.ListItinerario[i].Origen;
                    l_row["pv_destino"] = p_Model.ListItinerario[i].Destino;
                    l_row["pv_fecha_sol"] = p_Model.ListItinerario[i].FechaHora;
                    p_Model.DtItinerario.Rows.Add(l_row);
                    l_row = null;
                }

                p_Model.DtHospedaje = new DataTable();
                p_Model.DtHospedaje.Columns.Add("pv_codi_auho");
                p_Model.DtHospedaje.Columns.Add("pv_codi_dest");
                p_Model.DtHospedaje.Columns.Add("pv_fecha_inicio_hosp");
                p_Model.DtHospedaje.Columns.Add("pv_fecha_fin_hosp");

                for (var i = 0; i < p_Model.ListHospedaje.Count; i++)
                {
                    l_row = p_Model.DtHospedaje.NewRow();
                    l_row["pv_codi_auho"] = p_Model.ListHospedaje[i].CodiAuho;
                    l_row["pv_codi_dest"] = p_Model.ListHospedaje[i].CodiDest;
                    l_row["pv_fecha_inicio_hosp"] = p_Model.ListHospedaje[i].FInicio;
                    l_row["pv_fecha_fin_hosp"] = p_Model.ListHospedaje[i].FFin;
                    p_Model.DtHospedaje.Rows.Add(l_row);
                    l_row = null;
                }

                p_Model.DtComedor = new DataTable();
                p_Model.DtComedor.Columns.Add("pv_codi_auco");
                p_Model.DtComedor.Columns.Add("pv_fecha_inicio_come");
                p_Model.DtComedor.Columns.Add("pv_fecha_fin_come");
                p_Model.DtComedor.Columns.Add("pv_codi_comedor");
                p_Model.DtComedor.Columns.Add("pv_tipo_servicio");

                for (var i = 0; i < p_Model.ListComedor.Count; i++)
                {
                    l_row = p_Model.DtComedor.NewRow();
                    l_row["pv_codi_auco"] = p_Model.ListComedor[i].CodiAuco;
                    l_row["pv_fecha_inicio_come"] = p_Model.ListComedor[i].FDesde;
                    l_row["pv_fecha_fin_come"] = p_Model.ListComedor[i].FHasta;
                    l_row["pv_codi_comedor"] = p_Model.ListComedor[i].CodiCome;
                    l_row["pv_tipo_servicio"] = p_Model.ListComedor[i].CodiTise;
                    p_Model.DtComedor.Rows.Add(l_row);
                    l_row = null;
                }

                var l_validate = Dao.EditarAV(p_Model);

                foreach (DataRow row in l_validate.Rows)
                {
                    p_datos = new Response();
                    p_datos.Status = Convert.ToInt32(l_validate.Rows[0]["Value"]);
                    p_datos.Message = l_validate.Rows[0]["Mensaje"].ToString();
                    p_datos.Other = Convert.ToInt32(l_validate.Rows[0]["Other"].ToString());
                }

                ClearDataTable(l_validate);

                return p_datos;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private static DataTable GetListItinerario(ParmCodiAuvi p_Model)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CodiAuit");
            dt.Columns.Add("TipoVia");
            dt.Columns.Add("CodiRuco");
            dt.Columns.Add("Origen");
            dt.Columns.Add("Destino");
            dt.Columns.Add("FechaHoraSal");

            var l_records = Dao.GetListItinerario(p_Model);
            foreach (DataRow row in l_records.Rows)
            {
                var dr = dt.NewRow();
                dr[0] = row["CODI_AUIT"];
                dr[1] = row["TIPO_VIA"];
                dr[2] = row["CODI_RUCO"];
                dr[3] = row["ORIGEN"];
                dr[4] = row["DESTINO"];
                dr[5] = row["FECHA_HORA_SALIDA"];
                dt.Rows.Add(dr);
            }
            ClearDataTable(l_records);
            return dt;
        }

        private static DataTable GetListHospedaje(ParmCodiAuvi p_Model)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CodiAuho");
            dt.Columns.Add("CodiDest");
            dt.Columns.Add("Destino");
            dt.Columns.Add("FechaIni");
            dt.Columns.Add("FechaFin");

            var l_records = Dao.GetListHospedaje(p_Model);
            foreach (DataRow row in l_records.Rows)
            {
                var dr = dt.NewRow();
                dr[0] = row["CODI_AUHO"];
                dr[1] = row["CODI_DEST"];
                dr[2] = row["DESTINO"];
                dr[3] = row["FECHA_INICIO"];
                dr[4] = row["FECHA_FIN"];
                dt.Rows.Add(dr);
            }
            ClearDataTable(l_records);
            return dt;
        }

        private static DataTable GetListComedor(ParmCodiAuvi p_Model)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CodiAuco");
            dt.Columns.Add("FechaIni");
            dt.Columns.Add("FechaFin");
            dt.Columns.Add("CodiSede");
            dt.Columns.Add("Sede");
            dt.Columns.Add("CodiCome");
            dt.Columns.Add("Comedor");
            dt.Columns.Add("CodiTise");
            dt.Columns.Add("TipoServicio");

            var l_records = Dao.GetListComedor(p_Model);
            foreach (DataRow row in l_records.Rows)
            {
                var dr = dt.NewRow();
                dr[0] = row["CODI_AUCO"];
                dr[1] = row["FECHA_INICIO"];
                dr[2] = row["FECHA_FIN"];
                dr[3] = row["CODI_SEDE"];
                dr[4] = row["SEDE"];
                dr[5] = row["CODI_COME"];
                dr[6] = row["COMEDOR"];
                dr[7] = row["CODI_TISE"];
                dr[8] = row["TIPO_SERVICIO"];
                dt.Rows.Add(dr);
            }
            ClearDataTable(l_records);
            return dt;
        }

        public static Response BorrarAV(ParmCodiAuvi p_Model)
        {
            Response p_datos = null;
            try
            {
                var l_validate = Dao.BorrarAV(p_Model);
                foreach (DataRow row in l_validate.Rows)
                {
                    p_datos = new Response();
                    p_datos.Status = Convert.ToInt32(l_validate.Rows[0]["Value"]);
                    p_datos.Message = l_validate.Rows[0]["Mensaje"].ToString();
                }

                ClearDataTable(l_validate);

                return p_datos;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static Response AprobarAV(ParamAprobarAV p_Model)
        {
            Response p_datos = null;
            DataRow l_row;
            try
            {
                p_Model.DtMotivos = new DataTable();
                p_Model.DtMotivos.Columns.Add("pv_av_id");
                p_Model.DtMotivos.Columns.Add("pv_estado");
                p_Model.DtMotivos.Columns.Add("pv_motivo");

                for (var i = 0; i < p_Model.Motivos.Count; i++)
                {
                    l_row = p_Model.DtMotivos.NewRow();
                    l_row["pv_av_id"] = p_Model.Motivos[i].CodiAuvi;
                    l_row["pv_estado"] = p_Model.Motivos[i].Estado;
                    l_row["pv_motivo"] = p_Model.Motivos[i].Motivo;
                    p_Model.DtMotivos.Rows.Add(l_row);
                    l_row = null;
                }

                var l_validate = Dao.AprobarAV(p_Model);
                foreach (DataRow row in l_validate.Rows)
                {
                    p_datos = new Response();
                    p_datos.Status = Convert.ToInt32(l_validate.Rows[0]["Value"]);
                    p_datos.Message = l_validate.Rows[0]["Mensaje"].ToString();
                    p_datos.Other = Convert.ToInt32(l_validate.Rows[0]["Other"].ToString());
                }

                try
                {
                    if (p_datos.Status == 1000)
                    {
                        EmailHelper.SendEmailAutViaje(p_Model.Motivos);
                    }
                    else
                    {
                        p_datos.Message = "Error al enviar correos.";
                    }
                }
                catch (Exception e)
                {
                    p_datos.Message = "Error al enviar correos.";
                    //prevent exception
                }

                ClearDataTable(l_validate);

                return p_datos;
            }
            catch (Exception e)
            {
                throw;
            }
        }
        
        public static ResultPlanilla GetPlanilla(ParmCodiAuvi p_Model)
        {
            ResultPlanilla p_datos = null;
            var _result = Dao.GetPlanilla(p_Model);
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultPlanilla();
                    p_datos.CodiPlan = Convert.ToInt32(row["CODI_PLAN"]);
                    p_datos.CodiAuvi = Convert.ToInt32(row["CODI_AUVI"]);
                    p_datos.NroPlan = Convert.ToInt32(row["NRO_PLAN"]);
                    p_datos.FichaSap = row["FICHA_SAP"].ToString();
                    p_datos.CodiCeco = row["CODI_CECO"].ToString();
                    p_datos.Ceco = row["CECO"].ToString();
                    p_datos.FechaAdelanto = row["FECHA_ADELANTO"].ToString();
                    p_datos.FechaRegistro = row["FECHA_REGISTRO"].ToString();
                    p_datos.Estado = row["ESTADO"].ToString();
                    p_datos.Destino = row["DESTINO"].ToString();
                    p_datos.Simbolo = row["SIMBOLO"].ToString();
                    p_datos.Motivo = row["MOTIVO"].ToString();
                    p_datos.Nombre = row["NOMBRE"].ToString();
                    p_datos.Adelanto = Convert.ToDecimal(row["ADELANTO"]);
                    p_datos.ListComprobante = Helpers.GetComprobantes(p_datos.CodiPlan);
                }
                return p_datos;
            }
            catch
            {
                throw;
            }
        }

        private static DataTable GetComprobantes(int codi_plan)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CodiSust");
            dt.Columns.Add("CodiPlan");
            dt.Columns.Add("FechaDoc");
            dt.Columns.Add("CodiMotivo");
            dt.Columns.Add("Motivo");
            dt.Columns.Add("CodiTDoc");
            dt.Columns.Add("TipoDoc");
            dt.Columns.Add("NumDoc");
            dt.Columns.Add("Descripcion");
            dt.Columns.Add("Monto");

            var l_records = Dao.GetComprobantes(codi_plan);
            foreach (DataRow row in l_records.Rows)
            {
                var dr = dt.NewRow();
                dr[0] = row["CODI_SUST"];
                dr[1] = row["CODI_PLAN"];
                dr[2] = row["FECHA_DOCUMENTO"];
                dr[3] = row["CODI_MOTIVO"];
                dr[4] = row["MOTIVO"];
                dr[5] = row["CODI_TIPO_DOC"];
                dr[6] = row["TIPO_DOC"];
                dr[7] = row["NRO_DOCUMENTO_PAGO"];
                dr[8] = row["DESCRIPCION"];
                dr[9] = row["MONTO"];
                dt.Rows.Add(dr);
            }
            ClearDataTable(l_records);
            return dt;
        }

        public static List<ResultMotivo> GetPlanillaMotivo()
        {
            ResultMotivo p_datos = null;
            List<ResultMotivo> _list = new List<ResultMotivo>();
            var _result = Dao.GetPlanillaMotivo();
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultMotivo();
                    p_datos.CodiMotivo = Convert.ToInt32(row["CODI_MOTIVO"]);
                    p_datos.Descripcion = Convert.ToString(row["DESCRIPCION"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }

        public static List<ResultTipoDoc> GetPlanillaTipoDoc()
        {
            ResultTipoDoc p_datos = null;
            List<ResultTipoDoc> _list = new List<ResultTipoDoc>();
            var _result = Dao.GetPlanillaTipoDoc();
            try
            {
                foreach (DataRow row in _result.Rows)
                {
                    p_datos = new ResultTipoDoc();
                    p_datos.CodiTdoc = Convert.ToInt32(row["CODI_TIPO_DOC"]);
                    p_datos.Descripcion = Convert.ToString(row["DESCRIPCION"]);
                    _list.Add(p_datos);
                }
                return _list;
            }
            catch
            {
                throw;
            }
        }

        public static ResultTipoDoc GetTipoComprobante(ParmCodiTDoc param)
        {
            ResultTipoDoc p_datos = null;
            var _result = Dao.GetTipoComprobante(param);
            try
            {
                p_datos = new ResultTipoDoc();
                p_datos.CodiTdoc = Convert.ToInt32(_result.Rows[0]["CODI_TIPO_DOC"]);
                p_datos.Descripcion = Convert.ToString(_result.Rows[0]["DESCRIPCION"]);

                return p_datos;
            }
            catch
            {
                throw;
            }

        }

        public static ResultUserInfo GetUserInfo(Login_Model param)
        {
            ResultUserInfo p_datos = null;
            var _result = Dao.GetUserInfo(param);
            try
            {
                p_datos = new ResultUserInfo();
                p_datos.Nombres = Convert.ToString(_result.Rows[0]["NOMBRES"]);
                p_datos.FichaSap = Convert.ToString(_result.Rows[0]["FICHA_SAP"]);
                p_datos.Telefono = Convert.ToString(_result.Rows[0]["TELEFONO"]);
                p_datos.Ceco = Convert.ToString(_result.Rows[0]["CENTRO_COSTO"]);
                p_datos.Area = Convert.ToString(_result.Rows[0]["AREA"]);
                p_datos.SubArea = Convert.ToString(_result.Rows[0]["SUBAREA"]);

                return p_datos;
            }
            catch
            {
                throw;
            }

        }

        public static string GetParameterByKey(ParamKey param)
        {
            try
            {
                return Dao.GetParameterByKey(param.Key);
            }
            catch
            {
                throw;
            }
        }
        public static void ClearDataTable(DataTable p_dt)
        {
            if (p_dt != null)
            {
                p_dt.Clear();
                p_dt.Dispose();
                p_dt = null;
            }
        }
    }
}