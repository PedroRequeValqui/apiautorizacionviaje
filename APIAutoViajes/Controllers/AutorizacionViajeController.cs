﻿using APIAutoViajes.Models;
using System;
using System.Web.Http;

namespace APIAutoViajes.Controllers
{
    [Authorize]
    [RoutePrefix("api/autorizacionViaje")]
    public class AutorizacionViajeController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetId(int id)
        {
            var customerFake = "customer-fake";
            return Ok(customerFake);
        }

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            var customersFake = new string[] { "customer-1", "customer-2", "customer-3" };
            return Ok(customersFake);
        }

        [HttpPost]
        [Route("InsertAV")]
        public IHttpActionResult InsertarAutorizacionViajes(ParamInsertAV param)
        {
            try
            {
                return Ok(Helpers.InsertarAV(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("InsertPersona")]
        public IHttpActionResult InsertarPersona(ParamInsertPersona param)
        {
            try
            {
                return Ok(Helpers.InsertarPersona(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("InsertPlanilla")]
        public IHttpActionResult InsertarPlanilla(ParamInsertPlanilla param)
        {
            try
            {
                return Ok(Helpers.InsertarPlanilla(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("VerificarDiasHabiles")]
        public IHttpActionResult VerificarDiasHabiles(ParmDiasHabiles param)
        {
            try
            {
                return Ok(Helpers.VerificarDiasHabiles(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("VerificarDocumento")]
        public IHttpActionResult VerificarDocumento(ParmDocumento param)
        {
            try
            {
                return Ok(Helpers.VerificarDocumento(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("VerificarSustento")]
        public IHttpActionResult VerificarSustento(ParmSustento param)
        {
            try
            {
                return Ok(Helpers.VerificarSustento(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("RecuperarAV")]
        public IHttpActionResult RecuperarAV(ParmCodiAuvi param)
        {
            try
            {

                return Ok(Helpers.RecuperarAV(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("EditarAV")]
        public IHttpActionResult EditarAV(ParamInsertAV param)
        {
            try
            {
                return Ok(Helpers.EditarAV(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("BorrarAV")]
        public IHttpActionResult BorrarAV(ParmCodiAuvi param)
        {
            try
            {
                return Ok(Helpers.BorrarAV(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("AprobarAV")]
        public IHttpActionResult AprobarAV(ParamAprobarAV param)
        {
            try
            {
                return Ok(Helpers.AprobarAV(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("GetPlanilla")]
        public IHttpActionResult GetPlanilla(ParmCodiAuvi param)
        {
            try
            {

                return Ok(Helpers.GetPlanilla(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("GetParameterByKey")]
        public IHttpActionResult GetParameterByKey(ParamKey param)
        {
            try
            {
                return Ok(Helpers.GetParameterByKey(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }
    }
}
