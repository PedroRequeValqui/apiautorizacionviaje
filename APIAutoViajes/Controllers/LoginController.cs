﻿using APIAutoViajes.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace APIAutoViajes.Controllers
{
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        [HttpGet]
        [Route("echoping")]
        public IHttpActionResult EchoPing()
        {
            return Ok(true);
        }

        [HttpGet]
        [Route("echouser")]
        public IHttpActionResult EchoUser()
        {
            var identity = Thread.CurrentPrincipal.Identity;
            return Ok($" IPrincipal-user: {identity.Name} - IsAuthenticated: {identity.IsAuthenticated}");
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("authenticate")]
        public IHttpActionResult Authenticate(LoginRequest login)
        {
            if (login == null)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            //TODO: Validate credentials Correctly, this code is only for demo !!
            bool isCredentialValid = Dao.ValidateAccessTester(login);
            if (isCredentialValid)
            {
                var token = TokenGenerator.GenerateTokenJwt(login.Username);
                return Ok(token);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpPost]
        [Route("LoginUsuario")]
        public IHttpActionResult GetValidaUsuario(Login_Model p_Model)
        {
            try
            {
                var p_datos = new ResultLogin();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://servicios-ti.siderperu.pe/ApiGenericaSider/api/login/LoginWindows");

                    var postTask = client.PostAsJsonAsync<Login_Model>("", p_Model);
                    postTask.Wait();

                    var result = postTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<bool>();
                        readTask.Wait();

                        var login_ok = readTask.Result;
                        if (login_ok)
                        {
                            p_datos = Helpers.GetValidaUsuario(p_Model);
                        }
                        else
                        {
                            p_datos.Status = "-1";
                            p_datos.Message = "Usuario y/o Contraseña incorrectos.";
                        }
                    }
                    else
                    {
                        p_datos.Status = "-1";
                        p_datos.Message = "Hubo un error en la API.";
                    }
                }
                return Ok(p_datos);
            }
            catch (Exception ex)
            {
                return Ok(Helpers.GetException(ex));
            }
        }
    }
}
