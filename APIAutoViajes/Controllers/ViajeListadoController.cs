﻿using APIAutoViajes.Models;
using System;
using System.Data;
using System.Web.Http;

namespace APIAutoViajes.Controllers
{
    [Authorize]
    [RoutePrefix("api/Listado")]
    public class ViajeListadoController : ApiController
    {
        [HttpPost]
        [Route("ListInternoExterno")]
        public IHttpActionResult TrabajadorExterno(ParmListUsuario param)
        {
            try
            {

                return Ok(Helpers.GetListTrabajadorExterno(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListCentroCosto")]
        public IHttpActionResult CentroCosto(ParmBuscar p_buscar)
        {
            try
            {

                return Ok(Helpers.GetListCentroCosto(p_buscar));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListDestino")]
        public IHttpActionResult Destino(ParmDestino p_buscar)
        {
            try
            {

                return Ok(Helpers.GetListDestino(p_buscar));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListTipoDocumento")]
        public IHttpActionResult TipoDocumento()
        {
            try
            {

                return Ok(Helpers.GetListTipoDocumento());
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListSede")]
        public IHttpActionResult Sede()
        {
            try
            {

                return Ok(Helpers.GetListSede());
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListRutaComun")]
        public IHttpActionResult RutaComun(ParmRutaComun p_buscar)
        {
            try
            {

                return Ok(Helpers.GetListRutaComun(p_buscar));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListComedor")]
        public IHttpActionResult Comedor(ParmComedor param)
        {
            try
            {

                return Ok(Helpers.GetListComedor(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListTipoServicio")]
        public IHttpActionResult TipoServicio()
        {
            try
            {

                return Ok(Helpers.GetListTipoServicio());
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListPais")]
        public IHttpActionResult Pais(ParmPais param)
        {
            try
            {

                return Ok(Helpers.GetListPais(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListPep")]
        public IHttpActionResult Pep()
        {
            try
            {

                return Ok(Helpers.GetListPep());
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListPepDetalle")]
        public IHttpActionResult PepDetalle(ParmPepDetalle param)
        {
            try
            {

                return Ok(Helpers.GetListPepDetalle(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListAutViaje")]
        public IHttpActionResult GetListAV(ParamListAV param)
        {
            try
            {

                return Ok(Helpers.GetListAV(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListAprobarAV")]
        public IHttpActionResult GetListAprobarAV(ParmListApAv param)
        {
            try
            {

                return Ok(Helpers.GetListAprobarAV(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListUltimosViajes")]
        public IHttpActionResult GetUltimosViajes(ParmCodiAuvi param)
        {
            try
            {

                return Ok(Helpers.GetUltimosViajes(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListPlanillaMotivo")]
        public IHttpActionResult GetPlanillaMotivo()
        {
            try
            {
                return Ok(Helpers.GetPlanillaMotivo());
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("ListPlanillaTipoDoc")]
        public IHttpActionResult GetPlanillaTipoDoc()
        {
            try
            {

                return Ok(Helpers.GetPlanillaTipoDoc());
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("GetTipoComprobante")]
        public IHttpActionResult GetTipoComprobante(ParmCodiTDoc param)
        {
            try
            {

                return Ok(Helpers.GetTipoComprobante(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }

        [HttpPost]
        [Route("GetUserInfo")]
        public IHttpActionResult GetUserInfo(Login_Model param)
        {
            try
            {

                return Ok(Helpers.GetUserInfo(param));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }
    }
}
